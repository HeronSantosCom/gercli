unit untPrincipal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, TFlatPanelUnit, StdCtrls, DBCtrls, DB, Mask,
  TFlatEditUnit, ComCtrls, BmsXPPageControl, ADODB, LSFindReplaceDialogW,
  TFlatMemoUnit, TFlatMaskEditUnit, TFlatComboBoxUnit, TFlatGroupBoxUnit,
  dxCore, dxButton, SSBaseXP, BmsXPLabel, TFlatListBoxUnit, SSEdit, Grids,
  DBGrids;

type
  TfrmPrincipal = class(TForm)
    fplTopo: TFlatPanel;
    Image1: TImage;
    pgPrincipal: TBmsXPPageControl;
    tstCliente: TTabSheet;
    tstAgenciador: TTabSheet;
    adoConn: TADOConnection;
    adoQCliente: TADOQuery;
    btnCadastrar: TdxButton;
    btnEditar: TdxButton;
    btnDeletar: TdxButton;
    btnProcurar: TdxButton;
    fgbVinculo: TFlatGroupBox;
    lblVinculo: TLabel;
    lblMatricula: TLabel;
    fgbBancario: TFlatGroupBox;
    lblBanco: TLabel;
    lblAgencia: TLabel;
    lblConta: TLabel;
    fgbIdentificacao: TFlatGroupBox;
    lblRg: TLabel;
    lblExpedicao: TLabel;
    lblEmissor: TLabel;
    fgbPessoais: TFlatGroupBox;
    lblPai: TLabel;
    lblMae: TLabel;
    lblCivil: TLabel;
    lblNascimento: TLabel;
    lblSexo: TLabel;
    lblNome: TLabel;
    fgbContato: TFlatGroupBox;
    lblTelRes: TLabel;
    lblTelRec: TLabel;
    lblTelCom: TLabel;
    lblEndereco: TLabel;
    lblCpf: TLabel;
    lblAgenciador: TLabel;
    edtNome: TFlatEdit;
    edtMae: TFlatEdit;
    edtPai: TFlatEdit;
    cbxSexo: TFlatComboBox;
    edtNascimento: TFlatMaskEdit;
    cbxCivil: TFlatComboBox;
    edtEndereco: TFlatMemo;
    edtTelRes: TFlatMaskEdit;
    edtTelRec: TFlatMaskEdit;
    edtTelCom: TFlatMaskEdit;
    edtBanco: TFlatEdit;
    edtVinculoNome: TFlatEdit;
    edtEmissao: TFlatMaskEdit;
    edtEmissor: TFlatEdit;
    edtCpf: TFlatMaskEdit;
    cbxAgenciador: TFlatComboBox;
    btnConfirmar: TdxButton;
    btnCancelar: TdxButton;
    btnSair: TdxButton;
    adoQClienteId: TAutoIncField;
    adoQClienteNome: TWideStringField;
    adoQClienteNascimento: TWideStringField;
    adoQClienteCivil: TWideStringField;
    adoQClienteMae: TWideStringField;
    adoQClientePai: TWideStringField;
    adoQClienteEndereco: TMemoField;
    adoQClienteSexo: TWideStringField;
    adoQClienteTelRes: TWideStringField;
    adoQClienteTelRec: TWideStringField;
    adoQClienteTelCom: TWideStringField;
    adoQClienteCpf: TWideStringField;
    adoQClienteVinculoNome: TWideStringField;
    adoQClienteVinculoMat: TWideStringField;
    adoQClienteCodAgenciador: TIntegerField;
    adoQClienteBanco: TWideStringField;
    adoQClienteAgencia: TWideStringField;
    adoQClienteConta: TWideStringField;
    adoQClienteIdentidade: TWideStringField;
    adoQClienteIdentidadeDataEmissao: TWideStringField;
    adoQClienteIdentidadeEmissor: TWideStringField;
    edtAgencia: TFlatMaskEdit;
    edtConta: TFlatMaskEdit;
    edtMatricula: TFlatMaskEdit;
    edtIdentidade: TFlatMaskEdit;
    adoQAgenciador: TADOQuery;
    adoQAgenciadoragenciaId: TAutoIncField;
    adoQAgenciadoragenciaNome: TWideStringField;
    adoQAgenciadoragenciaCod: TIntegerField;
    tstNavegador: TTabSheet;
    BmsXPLabel1: TBmsXPLabel;
    lbxHideCodAgec: TFlatComboBox;
    BmsXPLabel2: TBmsXPLabel;
    flbNavegador: TFlatListBox;
    tstLogin: TTabSheet;
    FlatPanel1: TFlatPanel;
    btnLogin: TdxButton;
    edtSenha: TFlatEdit;
    edtUsuario: TFlatEdit;
    BmsXPLabel3: TBmsXPLabel;
    BmsXPLabel4: TBmsXPLabel;
    dsAgenciador: TDataSource;
    FlatPanel2: TFlatPanel;
    dbedtCodAgenciado: TDBEditXP;
    BmsXPLabel5: TBmsXPLabel;
    BmsXPLabel6: TBmsXPLabel;
    dbedtNomeAgenciado: TDBEditXP;
    FlatPanel3: TFlatPanel;
    dbGridAgec: TDBGrid;
    btnAgecConf: TdxButton;
    btnAgecCancel: TdxButton;
    btnAgecCad: TdxButton;
    btnAgecEdit: TdxButton;
    btnAgecDel: TdxButton;
    btnImprimir: TdxButton;
    procedure btnCadastrarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnProcurarClick(Sender: TObject);
    procedure edtNomeKeyPress(Sender: TObject; var Key: Char);
    procedure edtMatriculaKeyPress(Sender: TObject; var Key: Char);
    procedure tstClienteShow(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure cbxAgenciadorChange(Sender: TObject);
    procedure btnDeletarClick(Sender: TObject);
    procedure tstNavegadorShow(Sender: TObject);
    procedure edtCpfKeyPress(Sender: TObject; var Key: Char);
    procedure btnLoginClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure flbNavegadorClick(Sender: TObject);
    procedure btnAgecCadClick(Sender: TObject);
    procedure btnAgecCancelClick(Sender: TObject);
    procedure btnAgecConfClick(Sender: TObject);
    procedure btnAgecEditClick(Sender: TObject);
    procedure btnAgecDelClick(Sender: TObject);
    procedure tstAgenciadorShow(Sender: TObject);
    procedure dbedtNomeAgenciadoKeyPress(Sender: TObject; var Key: Char);
    procedure btnImprimirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;
  acaoConfirma, acaoAgenciador: Integer;

implementation

uses untDmDados, untImpressao;

{$R *.dfm}

procedure TfrmPrincipal.btnCadastrarClick(Sender: TObject);
begin
  btnCadastrar.Enabled := False;
  btnProcurar.Enabled := False;
  btnEditar.Enabled := False;
  btnDeletar.Enabled := False;

  btnConfirmar.Enabled := True;
  btnCancelar.Enabled := True;
  btnImprimir.Enabled := True;

  btnConfirmar.Default := True;

  acaoConfirma := 1;
  adoQCliente.Active := True;
  adoQCliente.Append;

  //APAGA OS CAMPOS
  lbxHideCodAgec.Text := '0';
  cbxAgenciador.Text := '';
  cbxSexo.Text := '';
  cbxCivil.Text := '';
  edtMae.Clear;
  edtNascimento.Clear;
  edtNome.Clear;
  edtPai.Clear;

  edtEndereco.Clear;
  edtTelCom.Clear;
  edtTelRec.Clear;
  edtTelRes.Clear;

  edtAgencia.Clear;
  edtBanco.Clear;
  edtConta.Clear;

  edtMatricula.Clear;
  edtVinculoNome.Clear;

  edtCpf.Clear;
  edtEmissao.Clear;
  edtEmissor.Clear;
  edtIdentidade.Clear;

  //HABILITA OS CAMPOS
  cbxAgenciador.Enabled := True;

  cbxCivil.Enabled := True;
  cbxSexo.Enabled := True;
  edtMae.Enabled := True;
  edtNascimento.Enabled := True;
  edtNome.Enabled := True;
  edtPai.Enabled := True;

  edtEndereco.Enabled := True;
  edtTelCom.Enabled := True;
  edtTelRec.Enabled := True;
  edtTelRes.Enabled := True;

  edtAgencia.Enabled := True;
  edtBanco.Enabled := True;
  edtConta.Enabled := True;

  edtMatricula.Enabled := True;
  edtVinculoNome.Enabled := True;

  edtCpf.Enabled := True;
  edtEmissao.Enabled := True;
  edtEmissor.Enabled := True;
  edtIdentidade.Enabled := True;

  cbxAgenciador.SetFocus;
end;

procedure TfrmPrincipal.btnCancelarClick(Sender: TObject);
begin
  adoQCliente.Cancel;

  cbxAgenciador.Enabled := False;

  cbxCivil.Enabled := False;
  cbxSexo.Enabled := False;
  edtMae.Enabled := False;
  edtNascimento.Enabled := False;
  edtNome.Enabled := False;
  edtPai.Enabled := False;

  edtEndereco.Enabled := False;
  edtTelCom.Enabled := False;
  edtTelRec.Enabled := False;
  edtTelRes.Enabled := False;

  edtAgencia.Enabled := False;
  edtBanco.Enabled := False;
  edtConta.Enabled := False;

  edtMatricula.Enabled := False;
  edtVinculoNome.Enabled := False;

  edtCpf.Enabled := False;
  edtEmissao.Enabled := False;
  edtEmissor.Enabled := False;
  edtIdentidade.Enabled := False;

  btnCadastrar.Enabled := True;
  btnProcurar.Enabled := True;
  btnEditar.Enabled := False;
  btnDeletar.Enabled := False;

  btnConfirmar.Enabled := False;
  btnCancelar.Enabled := False;
  btnImprimir.Enabled := False;

  btnConfirmar.Default := False;
end;

procedure TfrmPrincipal.btnConfirmarClick(Sender: TObject);
begin
  adoQCliente.FieldByName('CodAgenciador').Value := StrToInt(lbxHideCodAgec.Text);

  adoQCliente.FieldByName('Civil').Value := cbxCivil.Text;
  adoQCliente.FieldByName('Sexo').Value := cbxSexo.Text;
  adoQCliente.FieldByName('Mae').Value := edtMae.Text;
  adoQCliente.FieldByName('Nascimento').Value := edtNascimento.Text;
  adoQCliente.FieldByName('Nome').Value := edtNome.Text;
  adoQCliente.FieldByName('Pai').Value := edtPai.Text;

  adoQCliente.FieldByName('Endereco').Value := edtEndereco.Text;
  adoQCliente.FieldByName('TelCom').Value := edtTelCom.Text;
  adoQCliente.FieldByName('TelRec').Value := edtTelRec.Text;
  adoQCliente.FieldByName('TelRes').Value := edtTelRes.Text;

  adoQCliente.FieldByName('Agencia').Value := edtAgencia.Text;
  adoQCliente.FieldByName('Banco').Value := edtBanco.Text;
  adoQCliente.FieldByName('Conta').Value := edtConta.Text;

  adoQCliente.FieldByName('VinculoMat').Value := edtMatricula.Text;
  adoQCliente.FieldByName('VinculoNome').Value := edtVinculoNome.Text;

  adoQCliente.FieldByName('Cpf').Value := edtCpf.Text;
  adoQCliente.FieldByName('IdentidadeDataEmissao').Value := edtEmissao.Text;
  adoQCliente.FieldByName('IdentidadeEmissor').Value := edtEmissor.Text;
  adoQCliente.FieldByName('Identidade').Value := edtIdentidade.Text;

  case acaoConfirma of
    1: adoQCliente.Post;
    3: adoQCliente.UpdateRecord;
  end;

  cbxAgenciador.Enabled := False;

  cbxCivil.Enabled := False;
  cbxSexo.Enabled := False;
  edtMae.Enabled := False;
  edtNascimento.Enabled := False;
  edtNome.Enabled := False;
  edtPai.Enabled := False;

  edtEndereco.Enabled := False;
  edtTelCom.Enabled := False;
  edtTelRec.Enabled := False;
  edtTelRes.Enabled := False;

  edtAgencia.Enabled := False;
  edtBanco.Enabled := False;
  edtConta.Enabled := False;

  edtMatricula.Enabled := False;
  edtVinculoNome.Enabled := False;

  edtCpf.Enabled := False;
  edtEmissao.Enabled := False;
  edtEmissor.Enabled := False;
  edtIdentidade.Enabled := False;

  btnCadastrar.Enabled := True;
  btnProcurar.Enabled := True;
  btnEditar.Enabled := False;
  btnDeletar.Enabled := False;

  btnConfirmar.Enabled := False;
  btnCancelar.Enabled := False;
  btnImprimir.Enabled := False;
end;

procedure TfrmPrincipal.btnSairClick(Sender: TObject);
begin
  Halt;
end;

procedure TfrmPrincipal.btnProcurarClick(Sender: TObject);
begin
  btnCadastrar.Enabled := False;
  btnProcurar.Enabled := False;
  btnEditar.Enabled := True;
  btnDeletar.Enabled := True;

  btnConfirmar.Enabled := False;
  btnCancelar.Enabled := True;
  btnImprimir.Enabled := True;

  acaoConfirma := 2;
  adoQCliente.Active := True;

  edtNome.Enabled := True;
  edtMatricula.Enabled := True;
  edtCpf.Enabled := True;

  edtNome.SetFocus;
end;

procedure TfrmPrincipal.edtNomeKeyPress(Sender: TObject; var Key: Char);
begin
  if acaoConfirma = 2 Then
  begin
    if (key in [#13]) then
    begin
      adoQCliente.Locate('Nome',edtNome.Text,[loPartialKey,loCaseInsensitive]);
      if not adoQCliente.FieldByName('CodAgenciador').IsNull then
      begin
        adoQAgenciador.Locate('agenciaCod',adoQCliente.FieldByName('CodAgenciador').Value,[loPartialKey,loCaseInsensitive]);
        cbxAgenciador.ItemIndex := cbxAgenciador.Items.IndexOf(IntToStr(adoQAgenciador.FieldByName('agenciaCod').Value) + ' - ' + adoQAgenciador.FieldByName('agenciaNome').Value);
        lbxHideCodAgec.ItemIndex := cbxAgenciador.ItemIndex;
      end;
      if not adoQCliente.FieldByName('Civil').IsNull then
        cbxCivil.Text := adoQCliente.FieldByName('Civil').Value;
      if not adoQCliente.FieldByName('Sexo').IsNull then
        cbxSexo.Text := adoQCliente.FieldByName('Sexo').Value;
      if not adoQCliente.FieldByName('Mae').IsNull then
        edtMae.Text := adoQCliente.FieldByName('Mae').Value;
      if not adoQCliente.FieldByName('Nascimento').IsNull then
        edtNascimento.Text := adoQCliente.FieldByName('Nascimento').Value;
      if not adoQCliente.FieldByName('Nome').IsNull then
        edtNome.Text := adoQCliente.FieldByName('Nome').Value
      else ShowMessage('O nome solicitado n�o consta no banco de dados!');
      if not adoQCliente.FieldByName('Pai').IsNull then
        edtPai.Text := adoQCliente.FieldByName('Pai').Value;

      if not adoQCliente.FieldByName('Endereco').IsNull then
        edtEndereco.Text := adoQCliente.FieldByName('Endereco').Value;
      if not adoQCliente.FieldByName('TelCom').IsNull then
        edtTelCom.Text := adoQCliente.FieldByName('TelCom').Value;
      if not adoQCliente.FieldByName('TelRec').IsNull then
        edtTelRec.Text := adoQCliente.FieldByName('TelRec').Value;
      if not adoQCliente.FieldByName('TelRes').IsNull then
        edtTelRes.Text := adoQCliente.FieldByName('TelRes').Value;

      if not adoQCliente.FieldByName('Agencia').IsNull then
        edtAgencia.Text := adoQCliente.FieldByName('Agencia').Value;
      if not adoQCliente.FieldByName('Banco').IsNull then
        edtBanco.Text := adoQCliente.FieldByName('Banco').Value;
      if not adoQCliente.FieldByName('Conta').IsNull then
        edtConta.Text := adoQCliente.FieldByName('Conta').Value;

      if not adoQCliente.FieldByName('VinculoMat').IsNull then
        edtMatricula.Text := adoQCliente.FieldByName('VinculoMat').Value;
      if not adoQCliente.FieldByName('VinculoNome').IsNull then
        edtVinculoNome.Text := adoQCliente.FieldByName('VinculoNome').Value;

      if not adoQCliente.FieldByName('Cpf').IsNull then
        edtCpf.Text := adoQCliente.FieldByName('Cpf').Value;
      if not adoQCliente.FieldByName('IdentidadeDataEmissao').IsNull then
        edtEmissao.Text := adoQCliente.FieldByName('IdentidadeDataEmissao').Value;
      if not adoQCliente.FieldByName('IdentidadeEmissor').IsNull then
        edtEmissor.Text := adoQCliente.FieldByName('IdentidadeEmissor').Value;
      if not adoQCliente.FieldByName('Identidade').IsNull then
        edtIdentidade.Text := adoQCliente.FieldByName('Identidade').Value;

      edtNome.SelectAll;
    end;
  end;
end;

procedure TfrmPrincipal.edtMatriculaKeyPress(Sender: TObject;
  var Key: Char);
begin
  if acaoConfirma = 2 Then
  begin
    if (key in [#13]) then
    begin
      adoQCliente.Locate('VinculoMat',edtMatricula.Text,[loPartialKey,loCaseInsensitive]);
      if not adoQCliente.FieldByName('CodAgenciador').IsNull then
      begin
        adoQAgenciador.Locate('agenciaCod',adoQCliente.FieldByName('CodAgenciador').Value,[loPartialKey,loCaseInsensitive]);
        cbxAgenciador.ItemIndex := cbxAgenciador.Items.IndexOf(IntToStr(adoQAgenciador.FieldByName('agenciaCod').Value) + ' - ' + adoQAgenciador.FieldByName('agenciaNome').Value);
        lbxHideCodAgec.ItemIndex := cbxAgenciador.ItemIndex;
      end;
      if not adoQCliente.FieldByName('Civil').IsNull then
        cbxCivil.Text := adoQCliente.FieldByName('Civil').Value;
      if not adoQCliente.FieldByName('Sexo').IsNull then
        cbxSexo.Text := adoQCliente.FieldByName('Sexo').Value;
      if not adoQCliente.FieldByName('Mae').IsNull then
        edtMae.Text := adoQCliente.FieldByName('Mae').Value;
      if not adoQCliente.FieldByName('Nascimento').IsNull then
        edtNascimento.Text := adoQCliente.FieldByName('Nascimento').Value;
      if not adoQCliente.FieldByName('Nome').IsNull then
        edtNome.Text := adoQCliente.FieldByName('Nome').Value;
      if not adoQCliente.FieldByName('Pai').IsNull then
        edtPai.Text := adoQCliente.FieldByName('Pai').Value;

      if not adoQCliente.FieldByName('Endereco').IsNull then
        edtEndereco.Text := adoQCliente.FieldByName('Endereco').Value;
      if not adoQCliente.FieldByName('TelCom').IsNull then
        edtTelCom.Text := adoQCliente.FieldByName('TelCom').Value;
      if not adoQCliente.FieldByName('TelRec').IsNull then
        edtTelRec.Text := adoQCliente.FieldByName('TelRec').Value;
      if not adoQCliente.FieldByName('TelRes').IsNull then
        edtTelRes.Text := adoQCliente.FieldByName('TelRes').Value;

      if not adoQCliente.FieldByName('Agencia').IsNull then
        edtAgencia.Text := adoQCliente.FieldByName('Agencia').Value;
      if not adoQCliente.FieldByName('Banco').IsNull then
        edtBanco.Text := adoQCliente.FieldByName('Banco').Value;
      if not adoQCliente.FieldByName('Conta').IsNull then
        edtConta.Text := adoQCliente.FieldByName('Conta').Value;

      if not adoQCliente.FieldByName('VinculoMat').IsNull then
        edtMatricula.Text := adoQCliente.FieldByName('VinculoMat').Value
      else ShowMessage('A matr�cula solicitada n�o consta no banco de dados!');
      if not adoQCliente.FieldByName('VinculoNome').IsNull then
        edtVinculoNome.Text := adoQCliente.FieldByName('VinculoNome').Value;

      if not adoQCliente.FieldByName('Cpf').IsNull then
        edtCpf.Text := adoQCliente.FieldByName('Cpf').Value;
      if not adoQCliente.FieldByName('IdentidadeDataEmissao').IsNull then
        edtEmissao.Text := adoQCliente.FieldByName('IdentidadeDataEmissao').Value;
      if not adoQCliente.FieldByName('IdentidadeEmissor').IsNull then
        edtEmissor.Text := adoQCliente.FieldByName('IdentidadeEmissor').Value;
      if not adoQCliente.FieldByName('Identidade').IsNull then
        edtIdentidade.Text := adoQCliente.FieldByName('Identidade').Value;

      edtMatricula.SelectAll;
    end;
  end;
end;

procedure TfrmPrincipal.tstClienteShow(Sender: TObject);
var
  codAgenciador, nomeAgenciador: String;
begin
  cbxAgenciador.Clear;
  lbxHideCodAgec.Clear;
  adoQAgenciador.Active := True;
  adoQAgenciador.First;
  while (not adoQAgenciador.Eof) do
  begin
    codAgenciador := adoQAgenciador.FieldByName('agenciaCod').Value;
    nomeAgenciador := adoQAgenciador.FieldByName('agenciaNome').Value;
    cbxAgenciador.Items.Add(codAgenciador + ' - ' + nomeAgenciador);
    lbxHideCodAgec.Items.Add(codAgenciador);
    adoQAgenciador.Next;
  end;
end;

procedure TfrmPrincipal.btnEditarClick(Sender: TObject);
begin
  btnCadastrar.Enabled := False;
  btnProcurar.Enabled := False;
  btnEditar.Enabled := False;
  btnDeletar.Enabled := False;

  btnConfirmar.Enabled := True;
  btnCancelar.Enabled := True;
  btnImprimir.Enabled := True;


  btnConfirmar.Default := True;

  acaoConfirma := 3;
  adoQCliente.Active := True;
  adoQCliente.Edit;

  //HABILITA OS CAMPOS
  cbxAgenciador.Enabled := True;

  cbxCivil.Enabled := True;
  cbxSexo.Enabled := True;
  edtMae.Enabled := True;
  edtNascimento.Enabled := True;
  edtNome.Enabled := True;
  edtPai.Enabled := True;

  edtEndereco.Enabled := True;
  edtTelCom.Enabled := True;
  edtTelRec.Enabled := True;
  edtTelRes.Enabled := True;

  edtAgencia.Enabled := True;
  edtBanco.Enabled := True;
  edtConta.Enabled := True;

  edtMatricula.Enabled := True;
  edtVinculoNome.Enabled := True;

  edtCpf.Enabled := True;
  edtEmissao.Enabled := True;
  edtEmissor.Enabled := True;
  edtIdentidade.Enabled := True;

  cbxAgenciador.SetFocus;
end;

procedure TfrmPrincipal.cbxAgenciadorChange(Sender: TObject);
begin
  lbxHideCodAgec.ItemIndex := cbxAgenciador.ItemIndex;
end;

procedure TfrmPrincipal.btnDeletarClick(Sender: TObject);
begin
  if MessageDlg('Voc� deseja remover: ' +  edtNome.Text + '?',mtWarning,[mbYes,mbNo],0) = mrYes then
  begin
    adoQCliente.Delete;

    //APAGA OS CAMPOS
    lbxHideCodAgec.Text := '';
    cbxAgenciador.Text := '';
    cbxSexo.Text := '';
    cbxCivil.Text := '';
    edtMae.Clear;
    edtNascimento.Clear;
    edtNome.Clear;
    edtPai.Clear;

    edtEndereco.Clear;
    edtTelCom.Clear;
    edtTelRec.Clear;
    edtTelRes.Clear;

    edtAgencia.Clear;
    edtBanco.Clear;
    edtConta.Clear;

    edtMatricula.Clear;
    edtVinculoNome.Clear;

    edtCpf.Clear;
    edtEmissao.Clear;
    edtEmissor.Clear;
    edtIdentidade.Clear;

    adoQCliente.First;

    btnCadastrar.Enabled := True;
    btnProcurar.Enabled := True;
    btnEditar.Enabled := False;
    btnDeletar.Enabled := False;

    btnConfirmar.Enabled := False;
    btnCancelar.Enabled := False;
    btnImprimir.Enabled := False;
  end;
end;

procedure TfrmPrincipal.tstNavegadorShow(Sender: TObject);
var
  nomeCliente, cpfCliente: String;
  strContador: Integer;
begin
  flbNavegador.Enabled := True;
  flbNavegador.Items.Clear;
  adoQCliente.Active := True;
  adoQCliente.First;
  flbNavegador.Items.Add('     CPF       |                                     NOME');
  flbNavegador.Items.Add('============== | =============================================================================');
  strContador := adoQCliente.RecordCount;
  while (not adoQCliente.Eof) do
  begin
    nomeCliente := adoQCliente.FieldByName('Nome').Value;
    cpfCliente := adoQCliente.FieldByName('Cpf').Value;
    if Length(cpfCliente) > 0 then
    begin
      cpfCliente := Copy(cpfCliente,1,3) + '.' + Copy(cpfCliente,4,3) + '.' + Copy(cpfCliente,7,3) + '-' + Copy(cpfCliente,10,2);
    end else cpfCliente := '000.000.000-00';
    flbNavegador.Items.Add(cpfCliente + ' | ' + nomeCliente);
    adoQCliente.Next;
  end;
  if not (strContador <> 0) then
  begin
    flbNavegador.Items.Add('                                 NENHUM CLIENTE CADASTRADO!');
    flbNavegador.Enabled := False;
  end;;
  flbNavegador.Items.Add('==============================================================================================');
end;

procedure TfrmPrincipal.edtCpfKeyPress(Sender: TObject; var Key: Char);
begin
  if acaoConfirma = 2 Then
  begin
    if (key in [#13]) then
    begin
      adoQCliente.Locate('Cpf',edtCpf.Text,[loPartialKey,loCaseInsensitive]);
      if not adoQCliente.FieldByName('CodAgenciador').IsNull then
      begin
        adoQAgenciador.Locate('agenciaCod',adoQCliente.FieldByName('CodAgenciador').Value,[loPartialKey,loCaseInsensitive]);
        cbxAgenciador.ItemIndex := cbxAgenciador.Items.IndexOf(IntToStr(adoQAgenciador.FieldByName('agenciaCod').Value) + ' - ' + adoQAgenciador.FieldByName('agenciaNome').Value);
        lbxHideCodAgec.ItemIndex := cbxAgenciador.ItemIndex;
      end;
      if not adoQCliente.FieldByName('Civil').IsNull then
        cbxCivil.Text := adoQCliente.FieldByName('Civil').Value;
      if not adoQCliente.FieldByName('Sexo').IsNull then
        cbxSexo.Text := adoQCliente.FieldByName('Sexo').Value;
      if not adoQCliente.FieldByName('Mae').IsNull then
        edtMae.Text := adoQCliente.FieldByName('Mae').Value;
      if not adoQCliente.FieldByName('Nascimento').IsNull then
        edtNascimento.Text := adoQCliente.FieldByName('Nascimento').Value;
      if not adoQCliente.FieldByName('Nome').IsNull then
        edtNome.Text := adoQCliente.FieldByName('Nome').Value;
      if not adoQCliente.FieldByName('Pai').IsNull then
        edtPai.Text := adoQCliente.FieldByName('Pai').Value;

      if not adoQCliente.FieldByName('Endereco').IsNull then
        edtEndereco.Text := adoQCliente.FieldByName('Endereco').Value;
      if not adoQCliente.FieldByName('TelCom').IsNull then
        edtTelCom.Text := adoQCliente.FieldByName('TelCom').Value;
      if not adoQCliente.FieldByName('TelRec').IsNull then
        edtTelRec.Text := adoQCliente.FieldByName('TelRec').Value;
      if not adoQCliente.FieldByName('TelRes').IsNull then
        edtTelRes.Text := adoQCliente.FieldByName('TelRes').Value;

      if not adoQCliente.FieldByName('Agencia').IsNull then
        edtAgencia.Text := adoQCliente.FieldByName('Agencia').Value;
      if not adoQCliente.FieldByName('Banco').IsNull then
        edtBanco.Text := adoQCliente.FieldByName('Banco').Value;
      if not adoQCliente.FieldByName('Conta').IsNull then
        edtConta.Text := adoQCliente.FieldByName('Conta').Value;

      if not adoQCliente.FieldByName('VinculoMat').IsNull then
        edtMatricula.Text := adoQCliente.FieldByName('VinculoMat').Value;
      if not adoQCliente.FieldByName('VinculoNome').IsNull then
        edtVinculoNome.Text := adoQCliente.FieldByName('VinculoNome').Value;

      if not adoQCliente.FieldByName('Cpf').IsNull then
        edtCpf.Text := adoQCliente.FieldByName('Cpf').Value
      else ShowMessage('O CPF solicitado n�o consta no banco de dados!');
      if not adoQCliente.FieldByName('IdentidadeDataEmissao').IsNull then
        edtEmissao.Text := adoQCliente.FieldByName('IdentidadeDataEmissao').Value;
      if not adoQCliente.FieldByName('IdentidadeEmissor').IsNull then
        edtEmissor.Text := adoQCliente.FieldByName('IdentidadeEmissor').Value;
      if not adoQCliente.FieldByName('Identidade').IsNull then
        edtIdentidade.Text := adoQCliente.FieldByName('Identidade').Value;

      edtCpf.SelectAll;
    end;
  end;
end;

procedure TfrmPrincipal.btnLoginClick(Sender: TObject);
begin
  if edtUsuario.Text <> 'administrador' then
    ShowMessage('NOME DO USU�RIO INCORRETO!')
  else
    if edtSenha.Text <> 'gercliadmin123' then
      ShowMessage('SENHA DO USU�RIO INCORRETA!')
    else
    begin
      tstNavegador.Show;

      edtUsuario.Enabled := False;
      edtSenha.Enabled := False;

      btnLogin.Caption := 'J� est� autenticado!';
      btnLogin.Enabled := False;

      tstAgenciador.Enabled := True;
      tstCliente.Enabled := True;
      tstNavegador.Enabled := True;
      tstLogin.Enabled := False;
    end;
end;

procedure TfrmPrincipal.FormShow(Sender: TObject);
begin
  tstLogin.Show;
  edtUsuario.SetFocus;
end;

procedure TfrmPrincipal.flbNavegadorClick(Sender: TObject);
var
  edtNomeChar: Char;
begin
  tstCliente.Show;
  btnProcurarClick(btnProcurar);
  edtNome.Text := Copy(flbNavegador.Items.ValueFromIndex[flbNavegador.ItemIndex],17,Length(flbNavegador.Items.ValueFromIndex[flbNavegador.ItemIndex]));
  edtNomeChar := Chr(13);
  edtNomeKeyPress(edtNome,edtNomeChar);
end;

procedure TfrmPrincipal.btnAgecCadClick(Sender: TObject);
begin
  dbedtCodAgenciado.Enabled := True;
  dbedtNomeAgenciado.Enabled := True;

  btnAgecCad.Enabled := False;
  btnAgecCad.Visible := False;
  btnAgecEdit.Enabled := False;
  btnAgecEdit.Visible := False;
  btnAgecDel.Enabled := False;
  btnAgecDel.Visible := False;

  btnAgecConf.Enabled := True;
  btnAgecConf.Visible := True;
  btnAgecCancel.Enabled := True;
  btnAgecCancel.Visible := True;

  btnAgecConf.Default := True;

  acaoAgenciador := 1;
  adoQAgenciador.Append;
  dbedtCodAgenciado.SetFocus;
end;

procedure TfrmPrincipal.btnAgecCancelClick(Sender: TObject);
begin
  dbedtCodAgenciado.Enabled := False;
  dbedtNomeAgenciado.Enabled := False;

  btnAgecCad.Enabled := True;
  btnAgecCad.Visible := True;
  btnAgecEdit.Enabled := True;
  btnAgecEdit.Visible := True;
  btnAgecDel.Enabled := True;
  btnAgecDel.Visible := True;

  btnAgecConf.Enabled := False;
  btnAgecConf.Visible := False;
  btnAgecCancel.Enabled := False;
  btnAgecCancel.Visible := False;

  adoQAgenciador.Cancel;
end;

procedure TfrmPrincipal.btnAgecConfClick(Sender: TObject);
begin
  dbedtCodAgenciado.Enabled := False;
  dbedtNomeAgenciado.Enabled := False;

  btnAgecCad.Enabled := True;
  btnAgecCad.Visible := True;
  btnAgecEdit.Enabled := True;
  btnAgecEdit.Visible := True;
  btnAgecDel.Enabled := True;
  btnAgecDel.Visible := True;

  btnAgecConf.Enabled := False;
  btnAgecConf.Visible := False;
  btnAgecCancel.Enabled := False;
  btnAgecCancel.Visible := False;

  case acaoAgenciador of
    1: adoQAgenciador.Post;
    2: adoQAgenciador.UpdateRecord;
  end;
end;

procedure TfrmPrincipal.btnAgecEditClick(Sender: TObject);
begin
  dbedtCodAgenciado.Enabled := True;
  dbedtNomeAgenciado.Enabled := True;

  btnAgecCad.Enabled := False;
  btnAgecCad.Visible := False;
  btnAgecEdit.Enabled := False;
  btnAgecEdit.Visible := False;
  btnAgecDel.Enabled := False;
  btnAgecDel.Visible := False;

  btnAgecConf.Enabled := True;
  btnAgecConf.Visible := True;
  btnAgecCancel.Enabled := True;
  btnAgecCancel.Visible := True;

  btnAgecConf.Default := True;

  acaoAgenciador := 2;
  adoQAgenciador.Edit;
  dbedtCodAgenciado.SetFocus;
end;

procedure TfrmPrincipal.btnAgecDelClick(Sender: TObject);
begin
  if MessageDlg('Voc� deseja remover: ' +  dbedtNomeAgenciado.Text + '?',mtWarning,[mbYes,mbNo],0) = mrYes then
  begin
    adoQAgenciador.Delete;
    adoQAgenciador.First;
  end;
end;

procedure TfrmPrincipal.tstAgenciadorShow(Sender: TObject);
begin
  if not (acaoAgenciador > 0) then
  begin
    btnAgecConf.Visible := False;
    btnAgecCancel.Visible := False;
  end;
end;

procedure TfrmPrincipal.dbedtNomeAgenciadoKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (key in [#13]) then
  begin
    btnAgecConfClick(btnAgecConf);
  end;
end;

procedure TfrmPrincipal.btnImprimirClick(Sender: TObject);
var tmpComercial,tmpCpf,tmpExpedicao,tmpNascimento,tmpRecados,tmpResidencial: String;
begin
  frmImpressao.qrlAgencia.Caption := edtAgencia.Text;
  frmImpressao.qrlBanco.Caption := edtBanco.Text;
  frmImpressao.qrlCivil.Caption := cbxCivil.Text;
  tmpComercial := edtTelCom.Text;
  frmImpressao.qrlComercial.Caption := '(' + copy(tmpComercial,1,2) + ') ' + copy(tmpComercial,3,4) + '-' + copy(tmpComercial,7,4);
  frmImpressao.qrlConta.Caption := edtConta.Text;
  tmpCpf := edtCpf.Text;
  frmImpressao.qrlCpf.Caption := copy(tmpCpf,1,3) + '.' + copy(tmpCpf,4,3) + '.' + copy(tmpCpf,7,3) + '-' + copy(tmpCpf,10,2);
  frmImpressao.qrlEmprego.Caption := edtVinculoNome.Text;
  tmpExpedicao := edtEmissao.Text;
  frmImpressao.qrlExpedicao.Caption := copy(tmpExpedicao,1,2) + '/' + copy(tmpExpedicao,3,2) + '/' + copy(tmpExpedicao,5,4);
  frmImpressao.qrlMae.Caption := edtMae.Text;
  frmImpressao.qrlMatricula.Caption := edtMatricula.Text;
  tmpNascimento := edtNascimento.Text;
  frmImpressao.qrlNascimento.Caption := copy(tmpNascimento,1,2) + '/' + copy(tmpNascimento,3,2) + '/' + copy(tmpNascimento,5,4);
  frmImpressao.qrlNome.Caption := edtNome.Text;
  frmImpressao.qrlOrgao.Caption := edtEmissor.Text;
  frmImpressao.qrlPai.Caption := edtPai.Text;
  tmpRecados := edtTelRec.Text;
  frmImpressao.qrlRecados.Caption := '(' + copy(tmpRecados,1,2) + ') ' + copy(tmpRecados,3,4) + '-' + copy(tmpRecados,7,4);
  tmpResidencial := edtTelRes.Text;
  frmImpressao.qrlResidencial.Caption := '(' + copy(tmpResidencial,1,2) + ') ' + copy(tmpResidencial,3,4) + '-' + copy(tmpResidencial,7,4);
  frmImpressao.qrlRg.Caption := edtIdentidade.Text;
  frmImpressao.qrlSexo.Caption := cbxSexo.Text;
  frmImpressao.qrlEndereco1.Caption := edtEndereco.Lines[0];
  frmImpressao.qrlEndereco2.Caption := edtEndereco.Lines[1];
  frmImpressao.qrlEndereco3.Caption := edtEndereco.Lines[2];
  frmImpressao.qrpImpressao.Preview;
end;

end.
