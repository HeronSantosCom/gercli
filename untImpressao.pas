unit untImpressao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QuickRpt, QRCtrls, ExtCtrls, StdCtrls;

type
  TfrmImpressao = class(TForm)
    qrpImpressao: TQuickRep;
    QRBand1: TQRBand;
    QRBand2: TQRBand;
    QRShape1: TQRShape;
    qrlNome: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel1: TQRLabel;
    QRShape2: TQRShape;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRShape3: TQRShape;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRLabel18: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel20: TQRLabel;
    qrlMae: TQRLabel;
    qrlPai: TQRLabel;
    qrlNascimento: TQRLabel;
    qrlSexo: TQRLabel;
    qrlCivil: TQRLabel;
    QRLabel25: TQRLabel;
    qrlResidencial: TQRLabel;
    qrlRecados: TQRLabel;
    qrlComercial: TQRLabel;
    QRLabel29: TQRLabel;
    qrlBanco: TQRLabel;
    qrlConta: TQRLabel;
    qrlAgencia: TQRLabel;
    QRLabel33: TQRLabel;
    qrlEmprego: TQRLabel;
    qrlMatricula: TQRLabel;
    QRLabel36: TQRLabel;
    qrlRg: TQRLabel;
    qrlExpedicao: TQRLabel;
    qrlOrgao: TQRLabel;
    qrlCpf: TQRLabel;
    qrlEndereco1: TQRLabel;
    qrlEndereco2: TQRLabel;
    qrlEndereco3: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel30: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmImpressao: TfrmImpressao;

implementation

{$R *.dfm}

end.
