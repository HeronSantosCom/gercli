unit untDmDados;

interface

uses
  SysUtils, Classes, DB, ADODB;

type
  TdmDados = class(TDataModule)
    adoConn: TADOConnection;
    adoQCliente: TADOQuery;
    adoQClienteclienteId: TAutoIncField;
    adoQClienteclienteNome: TWideStringField;
    adoQClienteclienteNascimento: TWideStringField;
    adoQClienteclienteCivil: TIntegerField;
    adoQClienteclienteMae: TWideStringField;
    adoQClienteclientePai: TWideStringField;
    adoQClienteclienteEndereco: TMemoField;
    adoQClienteclienteSexo: TIntegerField;
    adoQClienteclienteTelRes: TWideStringField;
    adoQClienteclienteTelRec: TWideStringField;
    adoQClienteclienteTelCom: TWideStringField;
    adoQClienteclienteCpf: TWideStringField;
    adoQClienteclienteVinculoNome: TWideStringField;
    adoQClienteclienteVinculoMat: TIntegerField;
    adoQClienteclienteCodAgenciador: TIntegerField;
    adoQClienteclienteBanco: TWideStringField;
    adoQClienteclienteAgencia: TIntegerField;
    adoQClienteclienteConta: TIntegerField;
    adoQClienteclienteIdentidade: TIntegerField;
    adoQClienteclienteIdentidadeDataEmissao: TWideStringField;
    adoQClienteclienteIdentidadeEmissor: TWideStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmDados: TdmDados;

implementation

{$R *.dfm}

end.
