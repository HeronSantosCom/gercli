object dmDados: TdmDados
  OldCreateOrder = False
  Left = 244
  Top = 155
  Height = 341
  Width = 475
  object adoConn: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=.\database.mdb;Pers' +
      'ist Security Info=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 21
    Top = 21
  end
  object adoQCliente: TADOQuery
    Active = True
    Connection = adoConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM cliente;')
    Left = 24
    Top = 72
    object adoQClienteclienteId: TAutoIncField
      FieldName = 'clienteId'
      ReadOnly = True
    end
    object adoQClienteclienteNome: TWideStringField
      FieldName = 'clienteNome'
      Size = 255
    end
    object adoQClienteclienteNascimento: TWideStringField
      FieldName = 'clienteNascimento'
      Size = 8
    end
    object adoQClienteclienteCivil: TIntegerField
      FieldName = 'clienteCivil'
    end
    object adoQClienteclienteMae: TWideStringField
      FieldName = 'clienteMae'
      Size = 255
    end
    object adoQClienteclientePai: TWideStringField
      FieldName = 'clientePai'
      Size = 255
    end
    object adoQClienteclienteEndereco: TMemoField
      FieldName = 'clienteEndereco'
      BlobType = ftMemo
    end
    object adoQClienteclienteSexo: TIntegerField
      FieldName = 'clienteSexo'
    end
    object adoQClienteclienteTelRes: TWideStringField
      FieldName = 'clienteTelRes'
      Size = 10
    end
    object adoQClienteclienteTelRec: TWideStringField
      FieldName = 'clienteTelRec'
      Size = 10
    end
    object adoQClienteclienteTelCom: TWideStringField
      FieldName = 'clienteTelCom'
      Size = 10
    end
    object adoQClienteclienteCpf: TWideStringField
      FieldName = 'clienteCpf'
      Size = 11
    end
    object adoQClienteclienteVinculoNome: TWideStringField
      FieldName = 'clienteVinculoNome'
      Size = 255
    end
    object adoQClienteclienteVinculoMat: TIntegerField
      FieldName = 'clienteVinculoMat'
    end
    object adoQClienteclienteCodAgenciador: TIntegerField
      FieldName = 'clienteCodAgenciador'
    end
    object adoQClienteclienteBanco: TWideStringField
      FieldName = 'clienteBanco'
      Size = 255
    end
    object adoQClienteclienteAgencia: TIntegerField
      FieldName = 'clienteAgencia'
    end
    object adoQClienteclienteConta: TIntegerField
      FieldName = 'clienteConta'
    end
    object adoQClienteclienteIdentidade: TIntegerField
      FieldName = 'clienteIdentidade'
    end
    object adoQClienteclienteIdentidadeDataEmissao: TWideStringField
      FieldName = 'clienteIdentidadeDataEmissao'
      Size = 8
    end
    object adoQClienteclienteIdentidadeEmissor: TWideStringField
      FieldName = 'clienteIdentidadeEmissor'
      Size = 255
    end
  end
end
