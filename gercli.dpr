program gercli;

uses
  Forms,
  untPrincipal in 'untPrincipal.pas' {frmPrincipal},
  untImpressao in 'untImpressao.pas' {frmImpressao};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'gerCli';
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.CreateForm(TfrmImpressao, frmImpressao);
  Application.Run;
end.
